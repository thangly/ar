import React, { useEffect, useState } from 'react'
import { BrowserRouter as Router, Switch, Route, useLocation } from 'react-router-dom'

import '@google/model-viewer'
import { ARLink } from '@real2u/react-ar-components'

import './App.css'

export default function App() {
   return (
      <Router>
         <Switch>
            <Route path='/:id'>
               <AR />
            </Route>
         </Switch>
      </Router>
   )
}

function AR() {
   const location = useLocation()
   const [fileAR, setFileAR] = useState({ glb: '', usdz: '' })

   useEffect(() => {
      const linkFile = location.pathname.split('file-ar=')[1]
      const glb = linkFile.split('---')[0]
      const usdz = linkFile.split('---')[1]

      setFileAR((f) => ({ ...f, glb, usdz }))
   }, [location])

   return (
      <ARLink glb={fileAR.glb} usdz={fileAR.usdz}>
         <span className='btn-link-ar'>AR</span>
      </ARLink>
   )
}
